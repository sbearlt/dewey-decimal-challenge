import codecs

fn = sys.argv[1]

with codecs.open(fn, mode='r', encoding='utf-16') as f:
    text = f.read()

with codecs.open(fn, mode='w', encoding='utf-8') as f:
    f.write(text)