'''Script to measure your progress with the Dewey Decimal Challenge

'''
import argparse
import os
import sys


def main(args):
    parser = get_parser()
    a = parser.parse_args(args)
    if a.LibraryThing:
        a.HTML = True
        lines = parse(**a.__dict__)
        fn = 'LibraryThing-output.txt'
        with open(fn, mode='w') as f:
            f.write('\n'.join(lines))
        os.startfile(fn)
    else:
        lines = parse(**a.__dict__)
        print '\n'.join(lines)


def get_parser():
    parser = argparse.ArgumentParser(
            description=__doc__.split('\n')[0], epilog='\n'.join(__doc__.split('\n')[1:]), 
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--sections', help='File with DDC sections and books read', default='sections.txt')
    parser.add_argument('--divisions', help='File with DDC divisions', default='divisions.txt')
    parser.add_argument('--classesfile', help='File with DDC classes', default='classes.txt')
    parser.add_argument('-c', '--classes', help='Classes to read', default='0123456789')
    parser.add_argument('--no-sections', help='Do not print sections', action='store_true')
    parser.add_argument('--no-divisions', help='Do not print divisions', action='store_true')
    parser.add_argument('--HTML', help='Output as LibraryThing-Talk-friendly HTML', action='store_true')
    parser.add_argument('--class-html', help='HTML tag for DDC classes', default='<b>')
    parser.add_argument('--division-html', help='HTML tag for DDC divisions', default='<u>')
    parser.add_argument('--restrict-to-read', help='Only show the DDC sections that you\'ve read a book for', action='store_true')
    parser.add_argument('--LibraryThing', help='The LibraryThing-friendly option; it, alone, will do everything you probably want', action='store_true')
    parser.add_argument('--count-stats', help='Count proportion of divisions', action='store_true')
    return parser


def parse(**options):
    dxx, ddx, ddd = get_data(**options)
    counts = get_counts(dxx, ddx, ddd)
    lines = get_output(ddd, counts, **options)
    if options['count_stats']:
        lines = []#str(counts)]
        clsss = counts['classes'].values()
        ncr = 0
        ncu = 0
        for clss in clsss:
            if '(0/' in clss:
                ncu += 1
            elif '(' in clss:
                ncr += 1
        lines.append('Classes: %d/%d (%d%%)' % (ncr, ncr + ncu, float(ncr) / (ncr + ncu) * 100.))

        divs = counts['divisions'].values()
        ndr = 0
        ndu = 0
        for div in divs:
            if '(0/' in div:
                ndu += 1
            elif '(' in div:
                ndr += 1
        lines.append('Divisions: %d/%d (%d%%)' % (ndr, ndr + ndu, float(ndr) / (ndr + ndu) * 100.))
        lines.append('Sections: %d/%d (%d%%)' % (counts['NR'], counts['N'], float(counts['NR']) / counts['N'] * 100))
    return lines


def get_data(classesfile='classes.txt', divisions='divisions.txt', sections='sections.txt', **kwargs):
    with open(classesfile, mode='r') as dxxf:
        with open(divisions, mode='r') as ddxf:
            with open(sections, mode='r') as dddf:
                dxx = [l.strip('\n') for l in dxxf.readlines() if l.strip('\n').strip()]
                ddx = [l.strip('\n') for l in ddxf.readlines() if l.strip('\n').strip()]
                ddd = [l.strip('\n') for l in dddf.readlines() if l.strip('\n').strip()]
    return dxx, ddx, ddd


def get_counts(dxx, ddx, ddd):
    N = 0
    NR = 0
    d = {}
    di = {}
    for l in dxx:
        n = 0
        nr = 0
        for li in [li_ for li_ in ddx if li_[0] == l[0]]:
            dii = {}
            ni = 0
            nri = 0
            for lii in [lii_ for lii_ in ddd if lii_[:2] == li[:2]]:
                if ' -- ' in lii:
                    nri += 1
                ni += 1
            di[li[:2]] = li + ' (%d/%d)' % (nri, ni)
            n += ni
            nr += nri
        d[l[0]] = l + ' (%d/%d)' % (nr, n)
        N += n
        NR += nr
    return {'N': N, 'NR': NR, 'classes': d, 'divisions': di}


def get_output(ddd, counts, classes='0123456789', no_sections=False, no_divisions=False,
               class_html='<b>', division_html='<u>', HTML=False, 
               restrict_to_read=False, **kwargs):
    d = counts['classes']
    di = counts['divisions']
    
    test = lambda l: True
    test_div = lambda l: True
    if restrict_to_read:
        test = lambda l: ' -- ' in l
        test_div = lambda l: not '(0/' in l
    
    sp = ' '
    c_pre = ''
    c_post = ''
    div_pre = ''
    div_post = ''
    if HTML:
        sp = '&nbsp;'
        if not no_sections:
            c_pre = class_html
            c_post = class_html.replace('<', '</')
            div_pre = division_html
            div_post = division_html.replace('<', '</')

    dl = []
    for c in sorted([c_ for c_ in d.values() if c_[0] in classes]):
        dl += [c_pre + c + c_post]
        if not no_divisions:
            dl += ['']
        for div in sorted([div_ for div_ in di.values() if div_[0] == c[0]]):
            if not no_divisions:
                if test_div(div):
                    dl += [(sp * 1) + div_pre + div + div_post]
            ldl0 = len(dl)
            if not no_sections:
                dl += [(sp * 2) + l for l in ddd if (l[:2] == div[:2] and test(l))] + ['']
            nsl = len(dl) - ldl0
            if nsl == 1 and restrict_to_read:
                dl = dl[:-1]
        if no_sections and not no_divisions:
            dl += ['']
    return dl


if __name__ == '__main__':
    main(sys.argv[1:])