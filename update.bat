python parse.py --LibraryThing
python parse.py > LT.txt
python parse.py --HTML > LT.html
python parse.py --HTML --restrict-to-read > LT-only-read.html
python parse.py --restrict-to-read > LT-only-read.txt
python parse.py --count-stats > stats.txt
python utf16to8.py LT.txt
python utf16to8.py LT.html
python utf16to8.py LT-only-read.html
python utf16to8.py LT-only-read.txt
python utf16to8.py stats.txt
git add .
git commit -m "update reading using update.bat"
git push origin --all
