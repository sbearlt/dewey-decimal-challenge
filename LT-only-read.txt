0   Computer science, information & general works (1/84)

 07  News media, journalism and publishing (1/10)
  070 Documentary media, educational media, news media; journalism; publishing -- <i>[Heroes]</i> by John Pilger

1   Philosophy and psychology (5/89)

 10  Philosophy (1/9)
  100 Philosophy and psychology -- <i>[History of Western Philosophy]</i> by Bertrand Russell

 15  Psychology (3/7)
  152 Perception, movement, emotions, and drives -- <i>[The Blue Day]</i> Book
  153 Mental processes and intelligence -- <b>[Thinking, Fast and Slow]</b> by Daniel Kahneman (added 28 Apr 2013)
  155 Differential and developmental psychology -- <i>[Still Procrastinating]</i> by Joseph Ferrari

 19  Modern Western philosophy (19th-century, 20th-century) (1/10)
  192 Modern Western philosophy of the British Isles -- <i>[Confessions of a Philosopher]</i> by Bryan Magee

2   Religion (2/88)

 22  Bible (1/10)
  220 The Bible (General) -- <i>[Good News Bible]</i>

 27  Christian church history (1/10)
  270 Church history -- <i>[The Reformation]</i> by Owen Chadwick

3   Social sciences (17/90)

 30  Social sciences, sociology & anthropology (2/8)
  304 Factors affecting social behavior -- <i>[Whole Earth Discipline]</i> by Stewart Brand
  306 Culture & institutions -- <i>[A Short History of Medieval Philosophy]</i> by Julius Weinberg

 32  Political science (2/9)
  320 Political science (Politics and government) -- <i>[Sketches from a life]</i> by George Kennan
  327 International relations -- <i>[Memoirs: 1925-1950]</i> by George Kennan

 33  Economics (5/10)
  330 Economics -- <i>[Whoops!]</i> by John Lanchester
  331 Labor economics -- <i>[The Making of the English Working Class]</i> by Edward Thompson
  332 Financial economics -- <i>[The Ascent of Money]</i> by Niall Ferguson
  333 Land economics -- <i>[On the margins of the good earth]</i> by D. W. Meinig
  338 Production -- <i>[No Logo]</i> by Naomi Klein

 34  Law (1/10)
  345 Criminal law -- <i>[Return of Martin Guerre]</i> by Natalie Zemon Davis

 35  Public administration (1/10)
  359 Sea (Naval) forces & warfare -- <i>[The Bounty]</i> by Richard Hough

 36  Social services; association (3/10)
  362 Social welfare problems & services -- <i>[Beyond Love]</i> by Dominique Lapierre
  363 Other social problems & services -- <i>[Five Past Midnight]</i> by Dominique Lapierre
  365 Penal & related institutions -- <i>[Papillon]</i> by Henri Charriere

 38  Commerce, communications, transport  (1/10)
  387 Water, air, space transportation -- <i>[Supership]</i> by Noel Mostert

 39  Customs, etiquette, folklore (2/8)
  392 Customs of life cycle & domestic life -- <i>[Blokes and Sheds]</i> by Mark Thomson
  398 Folklore -- <i>[First Sunrise]</i> by Ainslie Roberts

4   Language (2/85)

 42  English & Old English (2/8)
  427 English language variations -- <i>[Death Sentence : the decay of public language]</i> by Don Watson
  428 Standard English usage -- <i>[Eats, Shoots and Leaves]</i> by Lynne Truss

5   Science (25/96)

 50  Sciences (2/9)
  500 Natural sciences & mathematics -- <i>[Natural history of the Adelaide region]</i> by C. R. Twidale
  509 Historical, geographic, persons treatment -- [Science and Civilisation in China : Volume 1 : Introductory orientations] by Joseph Needham (1 May 2013)

 51  Mathematics (4/9)
  510 General works on mathematics -- <i>[100 Essential Things You Didn't Know You Didn't Know]</i>
  512 Algebra & number theory -- <i>[Elementary Linear Algebra]</i>
  515 Analysis -- <i>[Div, Grad, Curl And All That]</i>
  519 Probabilities & applied mathematics -- <i>[The Drunkard's Walk]</i> by Leonard Mlodinow

 52  Astronomy & allied sciences (2/9)
  520 Astronomy and allied sciences -- <i>[The Skywatcher's Handbook]</i>
  523 Specific celestial bodies & phenomena -- <i>[Meteorites, Ice, and Antarctica]</i> by William Cassidy

 53  Physics (4/10)
  530 General works on physics -- <i>[Conceptual Physics]</i>
  536 Heat -- <i>[Einstein's Refrigerator]</i> by Gino Segre
  537 Electricity & electronics -- <i>[Principles and applications of electromagnetic fields]</i>
  539 Modern physics -- <i>[Madame Curie]</i> by Eve Curie

 54  Chemistry & allied sciences (2/10)
  546 Inorganic chemistry -- <i>[A World on Fire : a Heretic, an Aristocrat, and the Race to Discover Oxygen]</i> by Joe Jackson
  549 Mineralogy -- <i>[Introduction to the rock forming minerals]</i>

 55  Earth sciences (5/10)
  550 General works on earth sciences -- <i>[The Mapping of Geological Structures]</i> by Ken McClay
  551 Geology, hydrology, meteorology -- <i>[The Age of the Earth]</i> by G Dalyrymple
  552 Petrology -- <i>[The Field Description of Igneous Rocks]</i>
  557 Earth sciences of North America -- <i>[Geology underfoot in Yosemite National Park]</i>
  559 Earth sciences of other areas -- <i>[A field guide to the geology of the Yorke Peninsula]</i>

 57  Life sciences (3/9)
  573 Physiological systems of animals -- <i>[The Rise and Fall of the Third Chimpanzee]</i> by Jared Diamond
  576 Genetics and evolution -- <i>[The Ancestor's Tale]</i> by Richard Dawkins
  577 Ecology -- <i>[The Revenge of Gaia]</i> by James Lovelock

 58  Plants (2/10)
  583 Dicotyledones -- <i>[Gum trees in South Australia]</i>
  585 Gymnospermae (Pinophyta) -- <i>[The Wild Trees]</i> by Richard Preston

 59  Zoological sciences/Animals (1/10)
  591 Zoology -- <i>[Last Chance to See...]</i> by Douglas Adams

6   Technology  (13/93)

 60  Technology (Applied sciences) (1/10)
  609 Historical, geographic, persons treatment -- <i>[Seven Wonders of the Industrial World]</i> by Deborah Cadbury

 61  Medical sciences; Medicine (5/9)
  611 Human anatomy, cytology, histology -- <i>[The Anatomist: A True Story of Gray's Anatomy]</i> by Bill Hayes
  612 Human physiology -- <i>[The Brain That Changes Itself]</i> by Norman Doidge
  613 Personal health and safety -- <i>[In Defense of Food: An Eater's Manifesto]</i> by Michael Pollan
  616 Diseases -- <i>[The Demon in the Freezer: A True Story]</i> by Richard Preston
  617 Miscellaneous branches of medicine; surgery -- <i>[The Century of the Surgeon]</i> by Jurgen Thorwald

 62  Engineering and allied operations (5/9)
  622 Mining and related operations -- <i>[Don't Tell Mum I Work on the Rigs]</i> by Paul Carter
  623 Military and nautical engineering -- <i>[The Making of the Atomic Bomb]</i> by Richard Rhodes
  624 Civil engineering -- <i>[Structures: Or Why Things Don't Fall Down]</i> by J. E. Gordon
  625 Engineering of railroads and roads -- <i>[Too Long in the Bush]</i> by Len Beadell
  629 Other branches of engineering -- <i>[Digital Apollo: Human and Machine in Spaceflight]</i> by David Mindell

 63  Agriculture (1/10)
  636 Animal husbandry -- <i>[Nim Chimpsky]</i> by Elizabeth Hess

 64  Home economics & family living (1/10)
  641 Food and drink -- <i>[Animal, Vegetable, Miracle]</i> by Barbara Kingsolver

7   Arts (3/94)

 74  Drawing and decorative arts  (1/9)
  741 Drawing and drawings -- <i>[The Authoritative Calvin and Hobbes]</i> by Bill Watterson

 75  Painting and paintings (1/9)
  759 Geographical, historical, areas, persons treatment -- <i>[The Dawn of Time]</i> by <i>[<i>[Charles P Mountford]</i>]</i>

 79  Recreational and performing arts (1/10)
  796 Athletic and outdoor sports and games -- <i>[Clarrie Grimmett: The Bradman of Spin]</i> by Ashley Mallett

8   Literature  (19/99)

 80  Literature, rhetoric & criticism  (1/10)
  808 Rhetoric and collections of literary texts from more than two literatures  -- <i>[The Salmon of Doubt]</i> by Douglas Adams

 81  American literature in English  (3/9)
  811 American poetry in English -- <i>[Leaves of Grass and Selected Prose]</i> by Walt Whitman
  813 American fiction in English -- [The Contract Surgeon] by Dan O'Brien (added 5 May 2013)
  818 American miscellaneous writings in English -- <i>[A Year in Thoreau's Journal: 1851]</i> by Henry David Thoreau

 82  English and Old English literatures (7/10)
  820 English and Old English literatures -- <i>[Reading Lolita in Tehran : A Memoir in Books]</i> by Azar Nafisi
  821 English poetry -- <i>[Selected poems]</i> by John Donne
  822 English drama -- <i>[The Merchant of Venice]</i> by <i>[<i>[William Shakespeare]</i>]</i>
  823 English fiction -- [Scaramouche] by Rafael Sabatini (added 28 Apr 2013)
  824 English essays -- <i>[Inside the Whale and Other Essays]</i> by George Orwell
  827 English humor and satire -- <i>[Gulliver's Travels and Other Writings]</i> by Jonathan Swift
  828 English miscellaneous writings -- <i>[I can jump puddles]</i> by Alan Marshall

 83  German and related literatures  (1/10)
  833 German fiction -- <i>[The Swiss Family Robinson]</i> by Johann Wyss

 84  Literatures of Romance languages (2/10)
  842 French drama -- <i>[Waiting for Godot]</i> by Samuel Beckett
  843 French fiction -- <i>[Memoirs of Hadrian]</i> by Marguerite Yourcenar

 85  Italian, Romanian, Rhaeto-Romanic  (1/10)
  853 Italian fiction -- <i>[The Name of the Rose]</i> by Umberto Eco

 87  Italic literatures; Latin literature (1/10)
  873 Latin epic poetry and fiction -- <i>[The Golden Ass]</i> by Lucius Apuleius

 88  Hellenic literatures; Classical Greek  (2/10)
  883 Classical Greek epic poetry and fiction -- <i>[The Odyssey: Revised Prose Translation]</i> by Homer
  889 Modern Greek literature -- <i>[Zorba the Greek]</i> by Nikos Kazantzakes

 89  Literatures of other languages (1/10)
  891 East Indo-European & Celtic literatures -- <i>[War and Peace]</i> by Leo Tolstoy

9   History, geography & biography (21/98)

 90  History (4/10)
  900 History, geography, and auxiliary disciplines -- <i>[Dancing With Strangers]</i> by Inga Clendinnen
  901 Philosophy and theory of history -- <i>[What Is History?]</i> by Edward Hallet Carr
  904 Collected accounts of events -- <i>[Quest for adventure]</i> by Chris Bonington
  909 World history -- <i>[The Giant Book Of The 20th Century]</i> by Jon E. Lewis

 91  Geography & travel  (5/10)
  910 Geography and travel -- <i>["Gypsy Moth" Circles the World]</i> by Sir Francis Chichester
  912 Graphic representations of surface of earth and of extraterrestrial worlds -- <i>[Penguin Atlas of Ancient History]</i> by Colin McEvedy 
  914 Geography of and travel in Europe -- <i>[Flowering of the Renaissance]</i> by Vincent Cronin
  917 Geography of and travel in North America -- <i>[The Maine Woods]</i> by Henry David Thoreau
  919 Geography of and travel in other parts of the world (including Pacific Ocean Islands) and of extraterrestrial words -- <i>[West of Centre: A Journey of Discovery into the Heartland of Australia]</i> by Ray Ericksen

 92  Biography, genealogy, insignia  (1/10)
  920 Biography, genealogy, insignia -- <i>[The Girl in the Picture]</i> by Denise Chong

 94  General history of Europe  (4/10)
  940 History of Europe and Western Europe (as a whole) -- <i>[Europe from the Renaissance to Wateroo]</i> by Robert Ergang
  944 History of Europe - France and Monaco -- <i>[Citizens: A Chronicle of the French Revolution]</i> by Simon Schama
  945 History of Italy, Italian Peninsula, and adjacent islands -- <i>[The Sicilian Vespers]</i> by Steven Runciman
  947 History of Eastern Europe - Russia -- <i>[The Russian Revolution]</i> by Alan Moorehead

 95  General history of Asia; Far East (2/10)
  951 History of China and adjacent areas -- <i>[Seven Years in Tibet]</i> by Heinrich Harrer
  958 History of Central Asia -- <i>[Over the Edge: The True Story of Four American Climbers' Kidnap and Escape in the Mountains of Central Asia]</i> by Greg Child

 97  General history of North America  (2/10)
  973 History of the United States -- <i>[The Boys of My Youth]</i> by Jo Ann Beard
  978 History of the Western United States -- <i>[Bury My Heart at Wounded Knee: Indian History of the American West]</i> by Dee Brown

 98  General history of South America  (1/10)
  982 History of Argentina -- <i>[Alive: The Story of the Andes Survivors]</i> by Piers Paul Read

 99  General history of other areas (2/8)
  993 History of New Zealand -- <i>[New Zealand, a short history]</i> by John Cawte Beaglehole
  994 History of Australia -- <i>[The explorers]</i> by William Joy

